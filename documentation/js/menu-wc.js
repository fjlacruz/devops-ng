'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">Documentacion</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Escribe para buscar"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Comenzando</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Descripción general
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>Léeme
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencias
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter additional">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#additional-pages"'
                            : 'data-target="#xs-additional-pages"' }>
                            <span class="icon ion-ios-book"></span>
                            <span>Additional documentation</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="additional-pages"' : 'id="xs-additional-pages"' }>
                                    <li class="link ">
                                        <a href="additional-documentation/a-title.html" data-type="entity-link" data-context-id="additional">A TITLE</a>
                                    </li>
                                    <li class="link ">
                                        <a href="additional-documentation/b-title.html" data-type="entity-link" data-context-id="additional">B TITLE</a>
                                    </li>
                                    <li class="chapter inner">
                                        <a data-type="chapter-link" href="additional-documentation/c-title.html" data-context-id="additional">
                                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#additional-page-2e406c797ff4f6d7a45ea10605cfa213"' : 'data-target="#xs-additional-page-2e406c797ff4f6d7a45ea10605cfa213"' }>
                                                <span class="link-name">C TITLE</span>
                                                <span class="icon ion-ios-arrow-down"></span>
                                            </div>
                                        </a>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="additional-page-2e406c797ff4f6d7a45ea10605cfa213"' : 'id="xs-additional-page-2e406c797ff4f6d7a45ea10605cfa213"' }>
                                            <li class="link for-chapter2">
                                                <a href="additional-documentation/c-title/c1-title.html" data-type="entity-link" data-context="sub-entity" data-context-id="additional">C1 TITLE</a>
                                            </li>
                                        </ul>
                                    </li>
                        </ul>
                    </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Módulos</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-b9093b6fd18133622d61a5d09aae8150"' : 'data-target="#xs-components-links-module-AppModule-b9093b6fd18133622d61a5d09aae8150"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Componentes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-b9093b6fd18133622d61a5d09aae8150"' :
                                            'id="xs-components-links-module-AppModule-b9093b6fd18133622d61a5d09aae8150"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavbarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NavbarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AppServerModule.html" data-type="entity-link">AppServerModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppServerModule-7af5a252e97af6fdb059ab754264a081"' : 'data-target="#xs-components-links-module-AppServerModule-7af5a252e97af6fdb059ab754264a081"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Componentes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppServerModule-7af5a252e97af6fdb059ab754264a081"' :
                                            'id="xs-components-links-module-AppServerModule-7af5a252e97af6fdb059ab754264a081"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CompoModule.html" data-type="entity-link">CompoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CompoModule-26212027e6428bcfd719140edb294964"' : 'data-target="#xs-components-links-module-CompoModule-26212027e6428bcfd719140edb294964"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Componentes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CompoModule-26212027e6428bcfd719140edb294964"' :
                                            'id="xs-components-links-module-CompoModule-26212027e6428bcfd719140edb294964"' }>
                                            <li class="link">
                                                <a href="components/CompoListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CompoListComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CompoRoutingModule.html" data-type="entity-link">CompoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/HomeModule.html" data-type="entity-link">HomeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HomeModule-a33b4bf8efdcee83f37e8210f3b8e3b7"' : 'data-target="#xs-components-links-module-HomeModule-a33b4bf8efdcee83f37e8210f3b8e3b7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Componentes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HomeModule-a33b4bf8efdcee83f37e8210f3b8e3b7"' :
                                            'id="xs-components-links-module-HomeModule-a33b4bf8efdcee83f37e8210f3b8e3b7"' }>
                                            <li class="link">
                                                <a href="components/HomeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HomeComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomeRoutingModule.html" data-type="entity-link">HomeRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ListadoModule.html" data-type="entity-link">ListadoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ListadoModule-402aa8c51ff8f2cc2efdd8952adf7ad9"' : 'data-target="#xs-components-links-module-ListadoModule-402aa8c51ff8f2cc2efdd8952adf7ad9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Componentes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ListadoModule-402aa8c51ff8f2cc2efdd8952adf7ad9"' :
                                            'id="xs-components-links-module-ListadoModule-402aa8c51ff8f2cc2efdd8952adf7ad9"' }>
                                            <li class="link">
                                                <a href="components/ListadoComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ListadoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ListadoRoutingModule.html" data-type="entity-link">ListadoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ListaTablaModule.html" data-type="entity-link">ListaTablaModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ListaTablaModule-cde6b66f50c4f08ca2daa4d300962d77"' : 'data-target="#xs-components-links-module-ListaTablaModule-cde6b66f50c4f08ca2daa4d300962d77"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Componentes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ListaTablaModule-cde6b66f50c4f08ca2daa4d300962d77"' :
                                            'id="xs-components-links-module-ListaTablaModule-cde6b66f50c4f08ca2daa4d300962d77"' }>
                                            <li class="link">
                                                <a href="components/DetalleComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DetalleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ListaTablaComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ListaTablaComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ListaTablaRoutingModule.html" data-type="entity-link">ListaTablaRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/TestingModule.html" data-type="entity-link">TestingModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TestingModule-224859c0a6120546ed938d1b349f1aae"' : 'data-target="#xs-components-links-module-TestingModule-224859c0a6120546ed938d1b349f1aae"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Componentes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TestingModule-224859c0a6120546ed938d1b349f1aae"' :
                                            'id="xs-components-links-module-TestingModule-224859c0a6120546ed938d1b349f1aae"' }>
                                            <li class="link">
                                                <a href="components/TestingComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TestingComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TestingRoutingModule.html" data-type="entity-link">TestingRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Clases</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Datos.html" data-type="entity-link">Datos</a>
                            </li>
                            <li class="link">
                                <a href="classes/Farmacia.html" data-type="entity-link">Farmacia</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Inyectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/MenuService.html" data-type="entity-link">MenuService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PreloadService.html" data-type="entity-link">PreloadService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ServicioService.html" data-type="entity-link">ServicioService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscelánea</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Cobertura de la documentación</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentación generada utilizando <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});