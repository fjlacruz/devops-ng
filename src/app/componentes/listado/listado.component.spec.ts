import { ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { ListadoComponent } from './listado.component';
import { ServicioService } from '../../services/servicio.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { throwError, of } from 'rxjs';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

describe('ListadoComponent', () => {
  let component: ListadoComponent;
  let fixture: ComponentFixture<ListadoComponent>;
  let httpTestingController: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListadoComponent],
      providers: [ServicioService],
      imports: [HttpClientModule, HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });



  it('Debe verificar la conexion con el servicio', inject(
    [ServicioService],
    (service: ServicioService) => {
      expect(service).toBeTruthy();
    }
  ));
  it('Debe cargar la lista de elementos', () => {
    const farmacias = [
      {
        fecha: '04-12-20201',
        local_id: '534',
        local_nombre: 'TORRES MPD',
        comuna_nombre: 'RECOLETA',
        localidad_nombre: 'RECOLETA',
        local_direccion: 'AVENIDA EL SALTO 2972',
        funcionamiento_hora_apertura: '10:30 hrs.',
        funcionamiento_hora_cierre: '19:30 hrs.',
        local_telefono: '+560225053570',
        local_lat: '-33.3996351',
        local_lng: '-70.62894990000001',
        funcionamiento_dia: 'viernes',
        fk_region: '7',
        fk_comuna: '122',
      },
    ];

    spyOn(component.ServicioService, 'listarFarmacias').and.returnValue(of({ farmacias }));
    component.cargarFarmacias1();
    expect(component.farmacias).toEqual(<any>{ farmacias });
    expect(component.error).toBeFalsy();

  });

  xit('Caso de error al consultar servicio', () => {
    const emsg = 'deliberate 404 error';
    const service: ServicioService = TestBed.get(ServicioService);

    service.listarFarmacias();

    const req = httpTestingController.expectOne('https://27f91e7b-22de-4eb8-b867-968a907dcb22.mock.pstmn.io');

    req.flush(emsg, { status: 404, statusText: 'Not Found' });

    expect(component.farmacias).toEqual([]);
    expect(component.error).toBeTruthy();
 });

});
