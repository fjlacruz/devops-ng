import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ServicioService } from '../../services/servicio.service';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { catchError, map, takeUntil } from 'rxjs/operators';

import { Farmacia } from '../../models/farmacia.model';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css'],
})
export class ListadoComponent implements OnInit, OnDestroy {
  farmacias: Farmacia[] = [];
  farmacias$: Observable<Farmacia[]>;
  error = false;
  p: number = 1;
  farmacia: string;
  public test;

  private unsubscribe$ = new Subject<void>();

  constructor(
    public ServicioService: ServicioService,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    //this.cargarFarmacias1();
    this.cargarFarmacias2();
    this.test = this.ServicioService.listarFarmacias().pipe(map((data) => { return data.data }));
    console.log(this.test);
  }

  cargarFarmacias1() {
    try {
      console.time('midiendo tiempo');
      this.ServicioService.listarFarmacias()
        .pipe(takeUntil(this.unsubscribe$)) // unsubscribe to prevent memory leak
        .subscribe((farmacias) => {
          this.farmacias = farmacias; // unwrap observable
          this.cd.markForCheck(); // trigger change detection
          console.timeEnd('midiendo tiempo');
        });
    } catch (e) {
      console.log(e);
    }
  }

  async cargarFarmacias2() {
    try {
      const farmacias = await this.ServicioService.listarFarmacias();
      this.farmacias$=farmacias;
      //console.log(this.farmacias$);
    } catch (e) {
      console.log(e);
      console.log('error')
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
