import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ListadoComponent } from './listado.component';
import { ListadoRoutingModule } from './listado-routing.module';

@NgModule({
  declarations: [ListadoComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    ListadoRoutingModule
  ]
})
export class ListadoModule { }
