import { Component, OnInit } from '@angular/core';
import { ServicioService } from '../../services/servicio.service';
import { Farmacia } from '../../models/farmacia.model';
import { Datos } from '../../models/datos.model';

@Component({
  selector: 'app-lista-tabla',
  templateUrl: './lista-tabla.component.html',
  styleUrls: ['./lista-tabla.component.css'],
})
export class ListaTablaComponent implements OnInit {
  farmacias: Farmacia[] = [];
  datos: Datos[] = [];
  repDatos: any;
  error = false;
  constructor(public ServicioService: ServicioService) {}

  ngOnInit() {
    this.cargarTabla();
  }

  async cargarTabla() {
    try {
      await this.ServicioService.listarDatos().subscribe(
        (datos) => {
          this.datos = datos.data;
          this.repDatos = datos;
        },
        (error) => {
          this.error = true;
        }
      );
    } catch (e) {
      console.log(e);
    }
  }
}
