import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ListaTablaComponent } from './lista-tabla.component';
import { DetalleComponent } from '../detalle/detalle.component';
import { ListaTablaRoutingModule } from './lista-tabla-routing.module';

import { QuicklinkModule } from 'ngx-quicklink';

@NgModule({
  declarations: [ListaTablaComponent,DetalleComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    ListaTablaRoutingModule,
    QuicklinkModule
  ]
})
export class ListaTablaModule { }
