import { ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { ListaTablaComponent } from './lista-tabla.component';
import { ServicioService } from '../../services/servicio.service';
import { HttpClientModule } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {  of } from 'rxjs';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('ListadoComponent', () => {
  let component: ListaTablaComponent;
  let fixture: ComponentFixture<ListaTablaComponent>;
  let httpTestingController: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListaTablaComponent],
      providers: [ServicioService],
      imports: [HttpClientModule, HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTablaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });



  it('Debe verificar la conexion con el servicio', inject(
    [ServicioService],
    (service: ServicioService) => {
      expect(service).toBeTruthy();
    }
  ));

  it('Debe cargar la lista de elementos', () => {
    const datos = {
      page: 7,
      per_page: 6,
      total: 12,
      total_pages: 2,
      data: [
        {
          id: 7,
          email: "michael.lawson@reqres.in",
          first_name: "Michael",
          last_name: "Lawson",
          avatar: "https://reqres.in/img/faces/7-image.jpg"
        },


      ],
      support: {
        url: "https://reqres.in/#support-heading",
        text: "To keep ReqRes free, contributions towards server costs are appreciated!"
      }
    };

    spyOn(component.ServicioService, 'listarDatos').and.returnValue(of({ datos }));
    component.cargarTabla();
    console.log(component.repDatos);
    expect(component.repDatos).toEqual(<any>{ datos });
    expect(component.error).toBeFalsy();

  });

  it('Caso de error al consultar servicio', () => {
    const emsg = 'deliberate 404 error';
    const service: ServicioService = TestBed.get(ServicioService);

    service.listarDatos();

    const req = httpTestingController.expectOne('https://reqres.in/api/users?page=2');
    //console.log( req.request);

    req.flush(emsg, { status: 404, statusText: 'Not Found' });

    expect(component.datos).toEqual([]);
    expect(component.error).toBeTruthy();
  });


});


