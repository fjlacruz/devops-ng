import { Component, OnInit } from '@angular/core';
import { addDays, format } from 'date-fns'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  date:string;
 time2:string;
  constructor() { }

  ngOnInit(): void {
    const addNewDays = addDays(new Date(),20);
    this.date = format(addNewDays, 'yyyy/MMMM/dd');

  }

}
