import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ServicioService } from '../../services/servicio.service';


@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {

  dato: any;
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  error = false;
  data:{};
  public datoForm: FormGroup;

  constructor(
    public fb: FormBuilder,
    public servicioService: ServicioService,
    public router: Router,
    public activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      (params) => {
        //console.log(params.id)
        this.id = params.id;
      }
    );
    this.cargarDetalle(this.id);

    this.datoForm = this.fb.group({

      last_name: ['', Validators.required],
      first_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });

  }

  cargarDetalle(id) {
    this.servicioService.listarDatosdetalle(id).subscribe(
      (dato) => {
        this.dato = dato;
        this.datoForm.setValue(
          {

            last_name: this.dato.data.last_name,
            first_name: this.dato.data.first_name,
            email: this.dato.data.email
          }
        );
        //console.log('resp:' + this.id + ' ' + this.email + ' ' + this.first_name + ' ' + this.last_name);
      },
      (error) => {
        this.error = true;
        //console.log(this.error);
      }
    );
  }

  verificaCampos(){

    const formdata:any={
      first_name:this.datoForm.value.first_name,
      last_name:this.datoForm.value.last_name,
      email:this.datoForm.value.email
    }

    this.data=formdata;
    return formdata;
  }

}
