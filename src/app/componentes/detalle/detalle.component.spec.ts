import { ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { DetalleComponent } from './detalle.component';
import { ServicioService } from '../../services/servicio.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {  ReactiveFormsModule, FormsModule } from '@angular/forms';
import {  of } from 'rxjs';




describe('DetalleComponent', () => {
  let component: DetalleComponent;
  let fixture: ComponentFixture<DetalleComponent>;
  let httpTestingController: HttpTestingController;
  let servicioService:ServicioService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DetalleComponent],
      providers: [ServicioService],
      imports: [HttpClientModule, HttpClientTestingModule, RouterTestingModule, ReactiveFormsModule, FormsModule],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
    httpTestingController = TestBed.inject(HttpTestingController);

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  afterEach(() => {
    fixture.destroy();
  });


  it('Debe crear el componente de detalle', () => {
    expect(component).toBeTruthy();
  });

  it('Debe verificar la conexion del componente de detalle con el servicio', inject(
    [ServicioService],
    (service: ServicioService) => {
      expect(service).toBeTruthy();
    }
  ));

  it('Deber crear un form con tres campos', () => {
    expect(component.datoForm.contains('first_name')).toBeTruthy();
    expect(component.datoForm.contains('last_name')).toBeTruthy();
    expect(component.datoForm.contains('email')).toBeTruthy();
  });

  it('El first_name debe ser obligatorio', () => {
    const control = component.datoForm.get('first_name');
    control.setValue('');
    expect(control.valid).toBeFalsy();
  });

  it('El last_name debe ser obligatorio', () => {
    const control = component.datoForm.get('last_name');
    control.setValue('');
    expect(control.valid).toBeFalsy();
  });
  it('El email debe ser obligatorio', () => {
    const control = component.datoForm.get('last_name');
    control.setValue('');
    expect(control.valid).toBeFalsy();
  });

  it('descripcion prueba', () => {
    const control = component.datoForm.get('email');
    control.setValue('idsistemas15@gmail.com');
    expect(control.valid).toBeTruthy();

  });
  it('form invalido cuando este vacio', () => {
    expect(component.datoForm.valid).toBeFalsy();
  });

  it('Debe cargar la lista de elementos', () => {
    const datos = {
      data: {
        id: 8,
        email: "charles.morris@reqres.in",
        first_name: "Charles",
        last_name: "Morris",
        avatar: "https://reqres.in/img/faces/5-image.jpg"
      },
      support: {
        url: "https://reqres.in/#support-heading",
        text: "To keep ReqRes free, contributions towards server costs are appreciated!"
      }
    };

    spyOn(component.servicioService, 'listarDatosdetalle').and.returnValue(of({ datos }));
    component.cargarDetalle(5);
    expect(component.dato).toEqual(<any>{ datos });
    expect(component.datoForm.value).toBeTruthy();
    expect(component.error).toBeFalsy();
  });


  xit('Caso de error al consultar servicio listarDatosdetalle', inject(
    [ServicioService],
    (service: ServicioService) => {
      const emsg = 'deliberate 404 error';
      const id='undefined';
      service.listarDatosdetalle(5);
      const req = httpTestingController.expectOne(`https://reqres.in/api/users/${id}`);
      req.flush(emsg, { status: 404, statusText: 'Not Found' });
      console.log(req.request.url);
      expect(req.request.body).toEqual(null);
    }
  ));

  xit('Debe enviar los datos del form', () => {
    let datosTest ={first_name: "", last_name: "", email: ""}
    //expect(component.verificaCampos()).toEqual(datosTest);

   //console.log(component.servicioService.listarDatosdetalle(5).pipe);

  });

});

