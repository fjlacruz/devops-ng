import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterLinkWithHref } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { NavbarComponent } from './navbar.component';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarComponent ],
      imports: [
        RouterTestingModule.withRoutes([])
      ],

    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debe existir la ruta Home', () => {
    const elementos = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref));
    let existe = false;
    for (const elem of elementos) {
      if (elem.attributes['ng-reflect-router-link'] === '/') {
        existe = true;
        break;
      }
    }
    expect(existe).toBeTruthy();
  });

  it('Debe existir la ruta Listado', () => {
    const elementos = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref));
    let existe = false;
    for (const elem of elementos) {
      console.log(elem.attributes['ng-reflect-router-link']);
      if (elem.attributes['ng-reflect-router-link'] === 'listado') {
        existe = true;
        break;
      }
    }
    expect(existe).toBeTruthy();
  });

  it('Debe existir la ruta Testing', () => {
    const elementos = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref));
    console.log(elementos);
    let existe = false;
    for (const elem of elementos) {
      if (elem.attributes['ng-reflect-router-link'] === 'testing') {
        existe = true;
        break;
      }
    }
    expect(existe).toBeTruthy();
  });

  it('Debe existir la ruta Tabla', () => {
    const elementos = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref));
    console.log(elementos);
    let existe = false;
    for (const elem of elementos) {
      if (elem.attributes['ng-reflect-router-link'] === 'tabla') {
        existe = true;
        break;
      }
    }
    expect(existe).toBeTruthy();
  });

});
