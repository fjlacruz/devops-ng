import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../services/menu.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  public menuItems: any[];
  constructor(private menuService: MenuService) {
    this.menuItems = menuService.menu;
    ///console.log(this.menuItems);
  }

  ngOnInit(): void {}
}
