import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TestingComponent } from './testing.component';
import { TestingRoutingModule } from './testing-routing.module';

@NgModule({
  declarations: [TestingComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    TestingRoutingModule
  ]
})
export class TestingModule { }
