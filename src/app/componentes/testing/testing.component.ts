import { Component, OnInit, ViewChild } from '@angular/core';


@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.css'],
})
export class TestingComponent implements OnInit {
  @ViewChild('result') result: any;
  constructor() {}

  ngOnInit(): void {}

  calcular(n1: number, n2: number): number {
    console.log(Number(n1) + Number(n2));
    return Number(n1) + Number(n2);
  }
  pirntAdd(n1: number, n2: number): any {
    this.result.nativeElement.value = this.calcular(n1, n2);
  }
}
