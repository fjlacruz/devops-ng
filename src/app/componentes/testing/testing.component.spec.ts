import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingComponent } from './testing.component';

describe('TestingComponent', () => {
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestingComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(async () => {
    (<HTMLInputElement>document.getElementById('n1')).value = '';
    (<HTMLInputElement>document.getElementById('n2')).value = '';
    document.getElementById('calc').click();
  });

  it('El componente se ha creado', () => {
    expect(component).toBeTruthy();
  });
  it('formulario', () => {
    (<HTMLInputElement>document.getElementById('n1')).value = '2';
    (<HTMLInputElement>document.getElementById('n2')).value = '2';
    document.getElementById('calc').click();
    expect((<HTMLInputElement>document.getElementById('result')).value).toBe(
      '4'
    );
  });
});