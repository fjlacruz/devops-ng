export class Farmacia {
  constructor(
    public fecha: string,
    public local_id: string,
    public local_nombre: string,
    public comuna_nombre: string,
    public localidad_nombre: string,
    public local_direccion: string,
    public funcionamiento_hora_apertura: string,
    public funcionamiento_hora_cierre: string,
    public local_telefono: string,
    public local_lat: string,
    public local_lng: string,
    public funcionamiento_dia: string,
    public fk_region: string,
    public fk_comuna: string
  ) {}
}
