export class Datos {
  constructor(
    public id: BigInteger,
    public email: string,
    public first_name: string,
    public last_name: string,
    public avatar: string
  ) {}
}
