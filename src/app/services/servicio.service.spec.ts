import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import {
    HttpClientTestingModule,
    HttpTestingController,
} from '@angular/common/http/testing';
import { ServicioService } from './servicio.service';

describe('ServicioService', () => {
    let service: ServicioService;
    let httpClientSpy: { get: jasmine.Spy };
    let injector: TestBed;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule, HttpClientTestingModule],
        });
        injector = getTestBed();
        httpMock = injector.get(HttpTestingController);
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
        service = new ServicioService(httpClientSpy as any);
    });

    it('Debe retornar los datos del servicio', () => {
        const service: ServicioService = TestBed.get(ServicioService);
        const farmacias = [
            {
                fecha: '04-12-20201',
                local_id: '534',
                local_nombre: 'TORRES MPD',
                comuna_nombre: 'RECOLETA',
                localidad_nombre: 'RECOLETA',
                local_direccion: 'AVENIDA EL SALTO 2972',
                funcionamiento_hora_apertura: '10:30 hrs.',
                funcionamiento_hora_cierre: '19:30 hrs.',
                local_telefono: '+560225053570',
                local_lat: '-33.3996351',
                local_lng: '-70.62894990000001',
                funcionamiento_dia: 'viernes',
                fk_region: '7',
                fk_comuna: '122',
            },
        ];
        service.listarFarmacias().subscribe(farmacias => {
            expect(farmacias).toEqual(farmacias);
            service.listarFarmacias();
            const req = httpMock.expectOne(
                'https://27f91e7b-22de-4eb8-b867-968a907dcb22.mock.pstmn.io'
            );
            console.log(req.request);
            expect(req.request).toBeTruthy();
            expect(req.request.method).toBe('GET');
            req.flush(farmacias);
            expect(req.request.responseType).toEqual('json');
        });
    });

});
