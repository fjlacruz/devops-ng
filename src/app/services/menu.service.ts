import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class MenuService {
  menu: any[] = [
    { titulo: 'Home', url: '/' },
    { titulo: 'Listado', url: 'listado' },
    { titulo: 'Testing', url: 'testing' },
    { titulo: 'Tabla', url: 'tabla' },
  ];
  constructor() {}
}
