import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
import { Farmacia } from '../models/farmacia.model';

@Injectable({
  providedIn: 'root',
})
export class ServicioService {
  constructor(private http: HttpClient) {}

  listarFarmacias(): Observable<any> {
    try {
      const url = `https://27f91e7b-22de-4eb8-b867-968a907dcb22.mock.pstmn.io`;
      return this.http.get(url)
      .pipe(tap((resp: any) => {
          //console.log(resp);
        })
      );
    } catch (e) {
      console.log(e);
    }
  }

  listarDatos(): Observable<any> {
    const url = `https://reqres.in/api/users?page=2`;
    return this.http.get(url);
  }

  listarDatosdetalle(id: number): Observable<any> {
    const url = `https://reqres.in/api/users/${id}`;
    return this.http.get(url);
  }
}
