import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { PreloadService } from './core/services/preload.service';
import { QuicklinkStrategy } from 'ngx-quicklink';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('../app/componentes/home/home.module').then((m) => m.HomeModule),
   // data: { preload: true },
  },
  {
    path: 'listado',
    loadChildren: () =>
      import('../app/componentes/listado/listado.module').then(
        (m) => m.ListadoModule
      ),

  },
  {
    path: 'tabla',
    loadChildren: () =>
      import('../app/componentes/lista-tabla/lista-tabla.module').then(
        (m) => m.ListaTablaModule
      ),
  },
  {
    path: 'testing',
    loadChildren: () =>
      import('../app/componentes/testing/testing.module').then(
        (m) => m.TestingModule
      ),
  },

  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '',
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
}),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
