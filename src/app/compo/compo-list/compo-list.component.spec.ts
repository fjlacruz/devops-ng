import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompoListComponent } from './compo-list.component';

describe('CompoListComponent', () => {
  let component: CompoListComponent;
  let fixture: ComponentFixture<CompoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompoListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
