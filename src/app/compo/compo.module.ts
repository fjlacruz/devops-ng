import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompoRoutingModule } from './compo-routing.module';
import { CompoListComponent } from './compo-list/compo-list.component';


@NgModule({
  declarations: [CompoListComponent],
  imports: [
    CommonModule,
    CompoRoutingModule
  ]
})
export class CompoModule { }
